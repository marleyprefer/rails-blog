class Post < ApplicationRecord
  Gutentag::ActiveRecord.call self
  belongs_to :user
  validates :title, presence: true
  validates :body, presence: true
  def tags_as_string
    tag_names.join(", ")
  end

  def tags_as_string=(string)
    self.tag_names = string.split(/,\s*/)
  end
end
