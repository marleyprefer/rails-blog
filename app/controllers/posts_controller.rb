class PostsController < ApplicationController
  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.where(:user_id => params[:user_id])
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    set_post
  end

  # GET /posts/new
  def new
    require_user
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
    require_user
    set_post
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    @post.user_id = params[:user_id]
    respond_to do |format|
      if @post.save
        format.html { redirect_to [current_user, @post], notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    require_user
    set_post
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to [current_user, @post], notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    require_user
    set_post
    @post.destroy
    respond_to do |format|
      format.html { redirect_to [current_user, @post], notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :body, :tags_as_string)
    end

    def require_user
      @user = User.find_by_id(params[:user_id])
      redirect_to '/404.html' if @user.nil? or !current_user
    end
end
