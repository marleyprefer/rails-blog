class HomeController < ApplicationController
  def index
    if params['search'].present?
      return @posts = Post
                 .where("title LIKE ?", "%#{params['search']}%")
                 .or(Post.where("body LIKE ?", "%#{params['search']}%"))
    end
    if params['tag'].present?
      return @posts = Post.tagged_with(:names => params['tag'].split(/,\s*/))
    end
    @posts = Post.all
  end
end
