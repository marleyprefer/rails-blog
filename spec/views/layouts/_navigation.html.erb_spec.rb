require 'rails_helper'

RSpec.describe "layouts/_navigation.html.erb", type: :view do
  context "always" do
    before do
      allow(view).to receive(:user_signed_in?).and_return(false)
      render
    end

    it "should contain the App name" do
      expect(rendered).to have_link("Blog", href: '/', class: "navbar-brand")
    end
  end

  context "when the user is signed in" do
    before do
      @user = FactoryBot.create(:user)
      allow(view).to receive(:user_signed_in?).and_return(true)
      allow(view).to receive(:current_user).and_return(@user)
      render
    end

    it "does not show the Sign up link" do
      expect(rendered).to_not have_link("Sign up")
    end

    it "does not show the Login link" do
      expect(rendered).to_not have_link("Login")
    end

    it "does show the user nav" do
      expect(rendered).to have_link(@user.name, href: '#', class: 'nav-link dropdown-toggle')
    end
  end

  context "when there is not a signed in user" do
    before do
      allow(view).to receive(:user_signed_in?).and_return(false)

      render
    end

    it "shows the Sign up link" do
      expect(rendered).to have_link("Sign up", href: new_user_registration_path, class: "nav-item")
    end

    it "shows the Login link" do
      expect(rendered).to have_link("Login", href: new_user_session_path, class: "nav-item")
    end

    it "does not show the user nav" do
      expect(rendered).to_not have_link("Log out", class: 'dropdown-item')
    end
  end
end
