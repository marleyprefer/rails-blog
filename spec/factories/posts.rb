FactoryBot.define do
  factory :post do
    title { Faker::Lorem.sentence }
    body { Faker::Lorem.paragraphs(4).map{|pr| "<p>#{pr}</p>"}.join }
    association :user, :validates
  end
end
