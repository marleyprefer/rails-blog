require "rails_helper"

RSpec.feature "Account management", type: :feature do
  feature "User registration" do
    before do
      visit new_user_registration_path
    end
    scenario "new user successfully registered" do
      within("form#new_user") do
        fill_in "Name", with: "Test Name"
        fill_in "Email", with: "test@example.com"
        fill_in "Password", with: "pass123"
        fill_in "Password confirmation", with: "pass123"

        click_on "Sign up"
      end
      expect(page).to have_content "You have signed up successfully."
    end

    scenario "new user fails registration when name is not given" do
      within("form#new_user") do
        fill_in "Email", with: "test@example.com"
        fill_in "Password", with: "pass123"
        fill_in "Password confirmation", with: "pass123"

        click_on "Sign up"
      end
      expect(page).to have_content "Name can't be blank"

    end

    scenario "new user fails registration when email is invalid" do
      within("form#new_user") do
        fill_in "Name", with: "Test Name"
        fill_in "Email", with: "testexample.com"
        fill_in "Password", with: "pass123"
        fill_in "Password confirmation", with: "pass123"

        click_on "Sign up"
      end
      expect(page).to have_content "Email is invalid"
    end

    scenario "new user fails registration when password is too short" do
      within("form#new_user") do
        fill_in "Name", with: "Test Name"
        fill_in "Email", with: "testexample.com"
        fill_in "Password", with: "pass"
        fill_in "Password confirmation", with: "pass"

        click_on "Sign up"
      end
      expect(page).to have_content "Password is too short"
    end
  end
end
