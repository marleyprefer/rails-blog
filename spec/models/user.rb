require "rails_helper"

RSpec.describe User, type: :model do
  subject {FactoryBot.build(:user)}

  it "is valid with valid attributes" do
    expect(subject).to be_valid
  end

  it "is not valid without an email" do
    subject.email = nil

    expect(subject).to_not be_valid
  end

  it "is not valid with a malformed email" do
    addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar_baz.com foo@bar+baz.com]
    addresses.each do |invalid_address|
      subject.email = invalid_address
      expect(subject).to_not be_valid
    end
  end

  it "is not valid without a name" do
    subject.name = nil

    expect(subject).to_not be_valid
  end
end
